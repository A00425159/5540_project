-- use test;

select concat(DATE_FORMAT(NOW(), "%a %b %e %H:%i:%S %Y"), ' - deleting data from volume, volume_article, article, article_author') as '' from dual;

delete from volume_article;
delete from volume;
delete from article_author;
delete from article;

/*******************/
-- existing tables


delete from magazine;
delete from author;

insert into magazine (name) values
("Theor. Comput. Sci."),
("Linguistics and Philosophy"),
("Lecture Notes in Computer Science");


insert into author (fname,lname,email) values
("Houda", "Abbad", "houda.abbad@lycos.com"),
("Hideo", "Bannai", "bannai@inf.kyushu-u.ac.jp"),
("Mutlu", "Beyazit", "beyazit@adt.upb.de"),
("Francine", "Blanchet-Sadri", "blanchet@uncg.edu"),
("Janusz", "Brzozowski", "brzozo@uwaterloo.ca"),
("Cezar", "Campeanu", "cezar@sun11.math.upei.ca"),
("Mathieu", "Caralp", "mathieu.caralp@lif.univ-mrs.fr"),
("Pascal", "Caron", "pascal.caron@univ-rouen.fr"),
("Jean-Marc", "Champarnaud", "Jean-Marc.Champarnaud@univ-rouen.fr"),
("Dmitry", "Chistikov", "dch@mpi-sws.org"),
("Christian", "Choffrut", "Christian.Choffrut@liafa.jussieu.fr"),
("Stefano", "Crespi-Reghizzi", "stefano.crespireghizzi@polimi.it"),
("Denis", "Debarbieux", "denis.debarbieux@inria.fr"),
("Pierpaolo", "Degano", "degano@di.unipi.it"),
("Akim", "Demaille", "akim.demaille@gmail.com"),
("Michael", "Domaratzki", "mdomarat@cs.umanitoba.ca"),
("Frank", "Drewes", "drewes@cs.umu.se"),
("Alexandre", "Duret-Lutz", "adl@lrde.epita.fr"),
("Gianluigi", "Ferrari", "giangi@di.unipi.it"),
("Olivier", "Gauwin", "olivier.gauwin@labri.fr"),
("Thomas", "Genet", "thomas.genet@irisa.fr"),
("Daniela", "Genova", "d.genova@unf.edu"),
("Yuri", "Gurevich", "gurevich@microsoft.com"),
("Yo-Sub", "Han", "emmous@cs.yonsei.ac.kr"),
("MdMahbubul", "Hasan", "shanto86@gmail.com"),
("Pierre-Cyrille", "Heam", "pheam@femto-st.fr"),
("Fritz", "Henglein", "henglein@diku.dk"),
("Jan", "Holub", "Jan.Holub@fit.cvut.cz"),
("Markus", "Holzer", "holzer@informatik.uni-giessen.de"),
("Tomohiro", "I", "tomohiro.i@inf.kyushu-u.ac.jp"),
("A.S.M.Sohidull", "Islam", "sohansayed@gmail.com"),
("Masami", "Ito", "ito@cc.kyoto-su.ac.jp"),
("Sebastian", "Jakobi", "sebastian.jakobi@informatik.uni-giessen.de"),
("Jozef", "Jirasek", "jozef.jirasek@upjs.sk"),
("Oscar", "Ibarra", "ibarra@cs.ucsb.edu"),
("Shunsuke", "Inenaga", "inenaga@inf.kyushu-u.ac.jp"),
("Alois", "Dreyfus", "alois.dreyfus@femto-st.fr"),
("Galina", "Jiraskova", "jiraskov@saske.sk"),
("Natasa", "Jonoska", "jonoska@math.usf.edu"),
("Helmut", "Jurgensen", "hjj@csd.uwo.ca"),
("Lila", "Kari", "lila@csd.uwo.ca"),
("Andrzej", "Kisielewicz", "andrzej.kisielewicz@gmail.com"),
("Sang-Ki", "Ko", "narame7@cs.yonsei.ac.kr"),
("Stavros", "Konstantinidis", "s.konstantinidis@smu.ca"),
("Olga", "Kouchnarenko", "olga.kouchnarenko@femto-st.fr"),
("Dexter", "Kozen", "kozen@cs.cornell.edu"),
("Wener", "Kuich", "kuich@tuwien.ac.at"),
("Natalia", "Kushik", "ngkushik@gmail.com"),
("Martin", "Kutrib", "kutrib@informatik.uni-giessen.de"),
("Tristan", "LeGall", "tristan.le-gall@cea.fr"),
("Axel", "Legay", "axel.legay@inria.fr"),
("Pawan", "Lingras", "pawan.lingras@smu.ca"),
("Norma", "Linney", "norma.linney@smu.ca"),
("Sylvain", "Lombardy", "Sylvain.Lombardy@labri.fr"),
("Eva", "Maia", "emaia@dcc.fc.up.pt"),
("Rupak", "Majumdar", "rupak@mpi-sws.org"),
("Andreas", "Malcher", "andreas.malcher@informatik.uni-giessen.de"),
("Andreas", "Maletti", "andreas.maletti@ims.uni-stuttgart.de"),
("Sebastian", "Maneth", "Sebastian.Maneth@gmail.com"),
("Denis", "Maurel", "denis.maurel@univ-tours.fr"),
("Carlo", "Mereghetti", "mereghetti@di.unimi.it"),
("Gianluca", "Mezzetti", "mezzetti@di.unipi.it"),
("Nelma", "Moreira", "nam@dcc.fc.up.pt"),
("Frantisek", "Mraz", "mraz@ksvi.ms.mff.cuni.cz"),
("Paul", "Muir", "muir@smu.ca"),
("Valerie", "Murat", "valerie.murat@inria.fr"),
("Joachim", "Niehren", "joachim.niehren@inria.fr"),
("Lasse", "Nielsen", "lasse.nielsen.dk@gmail.com"),
("Takaaki", "Nishimoto", "a32b16c4@gmail.com"),
("Friedrich", "Otto", "otto@theory.informatik.uni-kassel.de"),
("Beatrice", "Palano", "palano@dsi.unimi.it"),
("Giovanni", "Pighizzini", "pighizzini@dico.unimi.it"),
("Daniel", "Prusa", "prusapa1@cmp.felk.cvut.cz"),
("M.Sohel", "Rahman", "sohel.kcl@gmail.com"),
("Ian", "McQuillan", "mcquillan@cs.usask.ca"),
("George", "Rahonis", "grahonis@math.auth.gr"),
("Bala", "Ravikumar", "ravi@cs.sonoma.edu"),
("Daniel", "Reidenbach", "D.Reidenbach@lboro.ac.uk"),
("Rogerio", "Reis", "rvr@dcc.fc.up.pt"),
("Pierre-Alain", "Reynier", "pierre-alain.reynier@lif.univ-mrs.fr"),
("Jacques", "Sakarovitch", "sakarovitch@telecom-paristech.fr"),
("Michel", "Rigo", "m.rigo@ulg.ac.be"),
("Kai", "Salomaa", "ksalomaa@cs.queensu.ca"),
("Pierluigi", "San-Pietro", "pierluigi.sanpietro@polimi.it"),
("Porter", "Scobey", "porter.scobey@smu.ca"),
("Tom", "Sebastian", "tom.sebastian@inria.fr"),
("Ayon", "Sen", "ayonsn@gmail.com"),
("Geraud", "Senizergues", "ges@labri.fr"),
("Klaus", "Sutner", "sutner@cs.cmu.edu"),
("Marek", "Szykula", "marek.esz@gmail.com"),
("Masayuki", "Takeda", "takeda@inf.kyushu-u.ac.jp"),
("Jean-Marc", "Talbot", "jean-marc.talbot@lif.univ-mrs.fr"),
("Marc", "Tommasi", "Marc.Tommasi@univ-lille3.fr"),
("Mikhail", "Volkov", "Mikhail.Volkov@usu.ru"),
("Bruce", "Watson", "bruce@fastar.org"),
("Matthias", "Wendlandt", "matthias.wendlandt@informatik.uni-giessen.de"),
("Hsu-Chun", "Yen", "yen@cc.ee.ntu.edu.tw"),
("Nina", "Yevtushenko", "ninayevtushenko@yahoo.com"),
("Mohamed", "Zergaoui", "innovimax@gmail.com"),
("Alexander", "Okhotin", "alexander.okhotin@utu.fi");


/******************/


update author set lname = upper(lname), fname = upper(fname); -- for the joins with the data coming from the jsons 
select concat(DATE_FORMAT(NOW(), "%a %b %e %H:%i:%S %Y"), ' - increasing field sizes in author and magazine to support the data to be loaded.') as '' from dual;
alter table author modify column lname varchar(64); -- names found on the json files do not fit char(30)
alter table author modify column fname varchar(64); -- names found on the json files do not fit char(30)
alter table magazine modify column name varchar(128); -- names found on the json files do not fit char(50)


-- loads the magazines
select concat(DATE_FORMAT(NOW(), "%a %b %e %H:%i:%S %Y"), ' - loading magazine_staging table') as '' from dual;
drop table if exists magazine_staging;
create temporary table magazine_staging (name varchar(128)) engine=myisam;
load data local infile 'journals_extract.csv' into table magazine_staging fields terminated by ',' enclosed by '"'(name);
-- create temp indexes
create index mag_name on magazine(name);
create index mag_name_idx on magazine_staging(name);

select concat(DATE_FORMAT(NOW(), "%a %b %e %H:%i:%S %Y"), ' - loading from magazine_staging into magazine table') as '' from dual;
insert into magazine (name) select a.name from magazine_staging a where a.name not in (select name from magazine);


-- loads the authors
select concat(DATE_FORMAT(NOW(), "%a %b %e %H:%i:%S %Y"), ' - loading author_staging table') as '' from dual;
drop table if exists author_staging;
create temporary table author_staging (fname varchar(64), lname varchar(64), email varchar(64)) engine=myisam;
load data local infile 'authors.csv' into table author_staging fields terminated by ',' enclosed by '"' (fname, lname, email);
-- create temp indexes
create index auth_name_idx on author(fname, lname, _id);
create index auth_name_stg_idx on author_staging(fname, lname, author_id);

select concat(DATE_FORMAT(NOW(), "%a %b %e %H:%i:%S %Y"), ' - loading from author_staging into author table') as '' from dual;
insert into author (fname, lname, email)
select fname, lname, email from author_staging a
where not exists(select 1 from author b where a.fname = b.fname and a.lname = b.lname);

-- creates and loads a denormalized staging table contained articles, volumes, authors, etc
select concat(DATE_FORMAT(NOW(), "%a %b %e %H:%i:%S %Y"), ' - loading articles_staging table') as '' from dual;
drop table if exists articles_staging;
create temporary table articles_staging (
                                                    title varchar(512),
                                                    journal varchar(128),
                                                    volume char(16),
                                                    year int,
                                                    pages char(16),
                                                    fname varchar(64), 
                                                    lname varchar(64),
                                                    article_id int, author_id int, volume_id int)
;

-- loads the staging table
load data local infile 'articles_staging.csv'
into table articles_staging
fields terminated by ',' enclosed by '"' (title, journal, volume, year, pages, fname, lname);

-- creates indexes on the staging table
create index articles_staging_auth_name_idx on articles_staging(fname, lname, author_id);
create index articles_staging_title on articles_staging(title, article_id);

-- loads the volume ids back to the staging table
select concat(DATE_FORMAT(NOW(), "%a %b %e %H:%i:%S %Y"), ' - updating volume_ids in articles_staging table') as '' from dual;
update articles_staging a set volume_id = (select _id from magazine b where b.name = a.journal);

-- loads the author ids back to the staging table
select concat(DATE_FORMAT(NOW(), "%a %b %e %H:%i:%S %Y"), ' - updating author_id in articles_staging table') as '' from dual;
update articles_staging a set author_id = (select distinct(_id) from author b where b.fname = a.fname and b.lname = a.lname);

-- loads the articles from the staging table
create index article_title on article(title, a_id);
select concat(DATE_FORMAT(NOW(), "%a %b %e %H:%i:%S %Y"), ' - loading articles from articles_staging') as '' from dual;
insert into article (title) select distinct title from articles_staging;

-- loads the article ids back to the staging table
select concat(DATE_FORMAT(NOW(), "%a %b %e %H:%i:%S %Y"), ' - updating article_id in articles_staging table') as '' from dual;
update articles_staging a set article_id = (select distinct a_id from article b where b.title = a.title);

-- loads the article author relationship
select concat(DATE_FORMAT(NOW(), "%a %b %e %H:%i:%S %Y"), ' - loading article_author from articles_staging') as '' from dual;
insert into article_author (_id, a_id) select distinct author_id, article_id from articles_staging;

-- loads the volume data
select concat(DATE_FORMAT(NOW(), "%a %b %e %H:%i:%S %Y"), ' - loading volme from articles_staging table') as '' from dual;
insert into volume select distinct b._id, a.volume, a.year from articles_staging a, magazine b where a.volume_id = b._id order by 1, 2, 3;

-- loads the relationship between volume and articles
select concat(DATE_FORMAT(NOW(), "%a %b %e %H:%i:%S %Y"), ' - loading volume_article from articles_staging table') as '' from dual;
insert into volume_article (_id, volnum, vol_year, a_id, pg_nos)
select distinct volume_id, volume, year, article_id, pages from articles_staging where volume_id is not null;

-- drop temp tables and load indexes
select concat(DATE_FORMAT(NOW(), "%a %b %e %H:%i:%S %Y"), ' - dropping indexes') as '' from dual;
drop index mag_name on magazine;
drop index auth_name_idx on author;
drop index article_title on article;
drop index mag_name_idx on magazine_staging;
drop index auth_name_stg_idx on author_staging;
drop index articles_staging_auth_name_idx on articles_staging;
drop index articles_staging_title on articles_staging;

select concat(DATE_FORMAT(NOW(), "%a %b %e %H:%i:%S %Y"), ' - -- DONE LOADING --') as '' from dual;
select ' - ------------------------------------ ') as '' from dual;

select a.fname, a.lname, art.title, va.vol_year, va.volnum, va.pg_nos, m.name
from article art, article_author aa, author a, volume_article va, volume v, magazine m 
where art.a_id = aa.a_id
and aa._id = a._id
and a.fname = 'STAVROS' 
and a.lname = 'KONSTANTINIDIS'
and va.a_id = art.a_id
and va._id = v._id
and va.volnum = v.volnum
and va.vol_year = v.vol_year
and v._id = m._id;

