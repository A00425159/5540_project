
/*
 * original table in MYSQL
create table if not exists AUTHOR (
  _id INT not null auto_increment,
  lname varchar(30) not null,
  fname varchar(30),
  email varchar(50),
  primary key(_id)
) engine = innodb;
*/

db.author.drop()

db.createCollection( "author", {
   validator: { $jsonSchema: {
      bsonType: "object",
      required: [ "lname" ],
      properties: {
         lname: {
            bsonType: "string",
            description: "last name is required"
         },
         fname: {
            bsonType : "string",
            description: "first name"
         },
         email: {
            bsonType : "string",
            pattern : "[a-zA-Z0-9]+@.*\..*$",
            description: "email"
         },
       }
   } }
} )

// insert some data to verify
db.author.insert( { lname: "Arantes", fname: "Daniel", email: "danie@gmail.com" } )
db.author.insert( { lname: "LaPlante", fname: "Jen", email: "jenla@gmail.com" } )
db.author.insert( { lname: "Malone", fname: "Duane", email: "dumalo@gmail.com" } )
// need to avoid insertions of fields that mysql would not accept
db.author.insert( { lname: "MaloneMaloneMaloneMaloneMaloneMalone", fname: "Duane", email: "dumalo@gmail.com" } )
db.author.find()
