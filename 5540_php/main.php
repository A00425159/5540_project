
<html>
<head>
    <title>main.php</title>
    <link href="style.css" rel="stylesheet" type="text/css">
</head>
<body>

<h1>Welcome to the HSL Database</h1>
    <div class="row">
    <div class="column left" style="background-color:#f0f0f0;">
        <ul><a href="get_new_article_info.php"> Add an Article </a></ul>
        <hr>
        <ul><a href="get_new_customer_info.php"> Add a Customer </a></ul>
        <hr>
        <ul><a href="get_new_transaction_info.php"> Enter New Transaction </a></ul>
        <hr>
        <ul><a href="get_delete_transaction_info.php"> Delete Old Transaction </a></ul>
    </div>
    <div class="column right" style="background-color:#f0f0f0;">
        <h2></h2>
        <?php
        include 'get_table_name.php';
        include 'print_list_tables.php';
        ?>
    </div>
    </div>

</body>

</html>
