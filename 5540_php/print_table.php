<html>
<head>
<title> print_table.php </title>
<link href="style.css" rel="stylesheet" type="text/css">
</head>
<body>

<?php

//get table name that is passed in the form
$table = $_POST["table"];
//convert to uppercase to make Linux happy
$table = strtolower($table);  

function prcols($table) {
	print "<table border=2>\n";
	print "<tr bgcolor=\"#f0f0f0\">";
	while ($a_row = mysqli_fetch_row($table)) {
		print "<td>$$a_row[0]</td>";
	}
	print "</tr>";
	print "</table>";
}

function prtable($table) {
	print "<table border=1>\n";
	while ($a_row = mysqli_fetch_row($table)) {
		print "<tr>";
		foreach ($a_row as $field) print "<td>$field</td>";
		print "</tr>";
	}
	print "</table>";
}

require("dbguest.php");
$link = mysqli_connect($host, $user, $pass, $db);
//Check the connection. Give error message if any error 
if (!$link) die("Couldn't connect to MySQL");

//Connect to db
mysqli_select_db($link, $db)
	or die("Couldn't open $db: ".mysqli_error($link));

$query = "SHOW COLUMNS FROM $table";
$result = mysqli_query($link, $query);
prcols($result);

$query = "select * from $table";
$result = mysqli_query($link, $query);

if (!$result) print("Table is empty.");
else {
	$num_rows = mysqli_num_rows($result);
	prtable($result);
	print "There are $num_rows rows in the table<p>";
}

?>

<p>
<a href="main.php"> back to MAIN menu</a>

</body>
</html>


