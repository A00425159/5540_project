<html>
	<head>
		<title>
			add_transaction.php
		</title>
	</head>
<body>

<?php
//Get data about new Transaction to add to the database
$c_id = $_POST["c_id"];
$_id =  $_POST["_id"];
$date = date("Y-m-d");
$id_list = [];
//$price_list = array();

require("dbguest.php");

$link = mysqli_connect($host, $user, $pass);
if (!$link) die("Couldn't connect to MySQL");

mysqli_select_db($link, $db)
	or die("Couldn't open $db: ".mysqli_error($link));

//check if provided Customer first and last name already exist
$query = "SELECT * FROM customer WHERE c_id = $c_id;";
$result = mysqli_query($link, $query);
if (!$result) {
	print("ERROR: ".mysqli_error($link)."<br><br>");
}
else if (mysqli_affected_rows($link) == 0) {
    print("The provided Customer ID is not valid."."<br>");
}
//if customer ID is valid then INSERT new transaction data
else if (mysqli_affected_rows($link) == 1) {
    $total = 0;
    foreach ($_id as &$value) {
        if ($value == "") break;
        $value_int = intval($value);
        //$id_list[] = $value_int;
        print("$value_int - ");
        $query = "SELECT price FROM item WHERE _id = $value_int;";
        $result = mysqli_query($link, $query);
        if (!$result) {
            print("ERROR: ".mysqli_error($link));
        }
        else {
            $price = mysqli_fetch_row($result);
            $id_list[] = $value_int;
            print("$"."$price[0]"."<br>");
            $id_price_list[$value] = $price[0];
            $total = $total + $price[0];
        }
    }
    print("---------------<br>Total: $$total"."<br>");
    print("Date: ".$date."<br><br>");
    //now insert data into the database
    $query = "BEGIN;";
    $result = mysqli_query($link, $query);
    $query = "INSERT INTO sale_transaction (t_date, total, discount, c_id) VALUES (NOW(), $total, 0, $c_id);";
    $result = mysqli_query($link, $query);
    if (!$result) {
        print("ERROR: ".mysqli_error($link));
    }
    $t_id = mysqli_insert_id($link);
    print("<br>Transaction added with ID number ".$t_id."<br><br>");
    //Now INSERT item details into sale_transaction_item table 
    $id_count = array_count_values($id_list);
    $id_list = array_unique($id_list);
    foreach($id_list as $value) {
        $query = "INSERT INTO sale_transaction_item (_id, t_id, price_at_sale, num_sold) VALUES ($value, $t_id, $id_price_list[$value], $id_count[$value]);";
        $result = mysqli_query($link, $query);
        if (!$result) {
            print("ERROR: ".mysqli_error($link));
        }
    } 
    $query = "COMMIT;";
    $result = mysqli_query($link, $query);
}
else {
    print("Something is not right.");
}
mysqli_close($link);

?>

<p>
<a href="main.php"> back to MAIN menu</a>

</body>
</html>