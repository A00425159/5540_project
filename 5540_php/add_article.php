<html>
	<head>
		<title>
			add_article.php
		</title>
	</head>
<body>

<?php
//Get data about new Article to add to the database
$title = $_POST["title"];
$_id = $_POST["_id"];
$volnum = $_POST["volnum"];
$year = $_POST["year"];
$pages = $_POST["pages"];
$lname = $_POST["lname"];
$fname = $_POST["fname"];
$email = $_POST["email"];

function prtable($table) {
	print "<table border=1>\n";
	while ($a_row = mysqli_fetch_row($table)) {
		print "<tr>";
		foreach ($a_row as $field) print "<td>$field</td>";
		print "</tr>";
	}
	print "</table>";
}

require("dbguest.php");

$link = mysqli_connect($host, $user, $pass);
if (!$link) die("Couldn't connect to MySQL");

mysqli_select_db($link, $db)
	or die("Couldn't open $db: ".mysqli_error($link));

//check if provided Magazine ID
$query = "SELECT * FROM magazine WHERE _id = $_id;";
$result = mysqli_query($link, $query);
if (!$result) {
	print("ERROR: ".mysqli_error($link)."<br><br>");
}
else if (mysqli_affected_rows($link) == 0) {
	print("The provided Magazine ID does not match our records.<br><br>");
}
//if magazine exist then INSERT the provided article information
else if (mysqli_affected_rows($link) == 1) {
	print("Magazine ID ".$_id." exists.<br><br>");
	$query = "BEGIN;";
	$result = mysqli_query($link, $query);
	$query = "INSERT INTO article (title) VALUES ('$title');";
	$result = mysqli_query($link, $query);
	if (!$result) {
		print("ERROR: ".mysqli_error($link));
	}
	else {
		$a_id = mysqli_insert_id($link);
		print "New article has id: " . $a_id . "<br><br>";
	}
	$query = "INSERT INTO volume (_id, volnum, vol_year) VALUES ($_id, $volnum, $year);";
	$result = mysqli_query($link, $query);
	if (!$result) {
		print("ERROR: ".mysqli_error($link));
	}
	else {
		print "Volume ".$volnum." added to Magazine ID ". $_id."<br><br>";
	}
	$query = "INSERT INTO volume_article (_id, volnum, vol_year, a_id, pg_nos) VALUES ($_id, $volnum, $year, $a_id, $pages);";
	$result = mysqli_query($link, $query);
	if (!$result) {
		print("ERROR: ".mysqli_error($link));
	}
	else {
		print "Article added to Magazine ID ". $_id. " Volume ". $volnum. ".<br><br>";
	}
	$query = "INSERT INTO author (lname, fname, email) VALUES ('$lname', '$fname', '$email');";
	$result = mysqli_query($link, $query);
	if (!$result) {
		print("ERROR: ".mysqli_error($link));
	}
	else {
		print "Author added to 'Author' table.<br><br>";
		$auth_id = mysqli_insert_id($link);
	}
	$query = "INSERT INTO article_author (_id, a_id) VALUES ($auth_id, $a_id);";
	$result = mysqli_query($link, $query);
	if (!$result) {
		print("ERROR: ".mysqli_error($link));
	}
	else {
		print "Author-Article table was updated.";
	}
	$query = "COMMIT;";
	$result = mysqli_query($link, $query);
}

mysqli_close($link);

?>

<p>
<a href="main.php"> back to MAIN menu</a>

</body>
</html>