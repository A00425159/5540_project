<html>
	<head>
		<title>
			add_customer.php
		</title>
	</head>
<body>

<?php
//Get data about new Customer to add to the database
$lname = $_POST["lname"];
$fname = $_POST["fname"];
$phone = $_POST["phone"];
$address = $_POST["address"];
$city = $_POST["city"];
$province = $_POST["province"];
$postal_code = $_POST["postal_code"];

function prtable($table) {
	print "<table border=1>\n";
	while ($a_row = mysqli_fetch_row($table)) {
		print "<tr>";
		foreach ($a_row as $field) print "<td>$field</td>";
		print "</tr>";
	}
	print "</table>";
}

require("dbguest.php");

$link = mysqli_connect($host, $user, $pass);
if (!$link) die("Couldn't connect to MySQL");

mysqli_select_db($link, $db)
	or die("Couldn't open $db: ".mysqli_error($link));

//check if provided Customer first and last name already exist
$query = "SELECT * FROM customer WHERE lname = '$lname' AND fname = '$fname';";
$result = mysqli_query($link, $query);
if (!$result) {
	print("ERROR: ".mysqli_error($link)."<br><br>");
}
else if (mysqli_affected_rows($link) >= 1) {
    print("There are ". mysqli_affected_rows($link). " customer(s) with the same name.<br>");
	print("Are you sure this is a new customer?");
	// print("<form action=\"add_customer.php\" method=\"POST\">");
	// print("<input type=\"text\" name=\"Y_N\" placeholder=\"y/n\">");
	// print("<input type=\"submit\" value=\"Submit\">");
	// if (isset($_POST['Y_N'])) {
	// 	$Y_N = $_POST["Y_N"];
	// 	print("$Y_N");
	// }
}
//if no results retuirned then INSERT new customer data
else if (mysqli_affected_rows($link) == 0) {
	$query = "INSERT INTO customer (lname, fname, phone, street_address, city, province, postal_code) VALUES ('$lname', '$fname', '$phone', '$address', '$city', '$province', '$postal_code');";
	$result = mysqli_query($link, $query);
	if (!$result) {
		print("ERROR: ".mysqli_error($link));
	}
	else {
		$c_id = mysqli_insert_id($link);
		print "New customer has id: " . $c_id . "<br><br>";
    }
}
else
    print("Something is not right.");

mysqli_close($link);

?>

<p>
<a href="main.php"> back to MAIN menu</a>

</body>
</html>