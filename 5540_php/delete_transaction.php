<html>
	<head>
		<title>
			delete_transaction.php
		</title>
	</head>
<body>

<?php
//Get data about Transaction to delete from the database
$t_id = $_POST["t_id"];

require("dbguest.php");

$link = mysqli_connect($host, $user, $pass);
if (!$link) die("Couldn't connect to MySQL");

mysqli_select_db($link, $db)
	or die("Couldn't open $db: ".mysqli_error($link));

//check if provided transaction ID exists
$query = "SELECT * FROM sale_transaction WHERE t_id = $t_id;";
$result = mysqli_query($link, $query);
if (!$result) {
	print("ERROR: ".mysqli_error($link)."<br><br>");
}
else if (mysqli_affected_rows($link) == 0) {
    print("The provided Transaction ID is not valid. ");
    print("<a href=\"get_delete_transaction_info.php\">Try again?</a>");
}
//if ID is valid then check the date and delete if < 30 days
else if (mysqli_affected_rows($link) == 1) {
    $row = mysqli_fetch_array($result);
    $t_datetime = new DateTime($row['t_date']);
    $t_date = $t_datetime->format('Y-m-d');
    $date1 = date_create($t_date);
    $today = date("Y-m-d");
    $date2 = date_create($today);
    $dates_diff = date_diff($date1, $date2);
    $dates_diff_str = $dates_diff->format('%a');
    $dates_diff_int = intval($dates_diff_str);
    if ($dates_diff_int <= 30) {
        // Date is ok, proceed to delete the transaction
        $query = "DELETE FROM sale_transaction_item WHERE t_id = $t_id;";
        $result = mysqli_query($link, "BEGIN;");
        $result = mysqli_query($link, $query);
        if (!$result) {
            print("ERROR: ".mysqli_error($link));
        }
        $query = "DELETE FROM sale_transaction WHERE t_id = $t_id;";
        $result = mysqli_query($link, $query);
        if (!$result) {
            print("ERROR: ".mysqli_error($link));
        }
        print("<br>Transaction ID ".$t_id." has been deleted.");
        $result = mysqli_query($link, "COMMIT;");
    }
}
else {
    print("Something is not right.");
}
mysqli_close($link);

?>

<p>
<a href="main.php"> back to MAIN menu</a>

</body>
</html>