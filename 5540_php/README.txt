*************************
dbguest.php is configured for a localhost MySQL connection. If you are running this on CDA540 server then you need to make the necessary changes to that file. 
*************************

April 3, 2018 STATUS
- Able to add a Transaction with a c_id and up to 10 _ids. Not able to store all items into the sale_transaction_item table. 


March 31, 2018 STATUS
- Make sure you've executed existing_tables.sql and new_tables.sql before testing the php interface. 
- main.phop will display all tables in the database
- you can enter a table name to display
- There is basic functionality for entering a new article and new customer. 
- There is some basic initial css styling. 
- There is a long way to go. 

