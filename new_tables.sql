drop table if exists sale_transaction_item;
drop table if exists sale_transaction;
drop table if exists customer;
drop table if exists volume_article;
drop table if exists volume;
drop table if exists article_author;
drop table if exists article;

drop table if exists customer;
create table if not exists customer (
  c_id int not null auto_increment,
  lname varchar(30) not null,
  fname varchar(30),
  phone varchar(10),
  street_address varchar(30),
  city varchar(30),
  province varchar(20),
  postal_code varchar(6),
  primary key(c_id)
) engine = innodb;


drop table if exists sale_transaction;
create table if not exists sale_transaction (
  t_id bigint not null auto_increment,
  t_date datetime not null,
  total float(8,2),
  discount float(3,2) default 0,
  c_id int not null,
  foreign key(c_id) references customer(c_id),
  primary key(t_id)
) engine = innodb;


drop table if exists sale_transaction_item;
create table if not exists sale_transaction_item (
  _id bigint not null,
  t_id bigint not null,
  price_at_sale float(8,2) not null,
  num_sold smallint,
  foreign key(_id) references item(_id),
  foreign key(t_id) references sale_transaction(t_id),
  primary key(_id, t_id)
) engine = innodb;


drop table if exists volume;
/*create table if not exists volume (
  _id int not null,
  volnum smallint not null,
  vol_year smallint,
  foreign key(_id) references magazine(_id),
  primary key(_id, volnum, vol_year)
) engine = innodb;
*/
-- year has to be part of the key and volume number has to be a char as there are some that are not numbers.
create table if not exists volume (
  _id int not null,
  volnum varchar(16) not null,
  vol_year smallint,
  foreign key(_id) references magazine(_id),
  primary key(_id, volnum, vol_year)
) engine = innodb;


drop table if exists article;
create table if not exists article (
  a_id int not null auto_increment,
  title varchar(300),
  primary key(a_id)
) engine = innodb;


drop table if exists article_author;
create table if not exists article_author (
  _id int not null,
  a_id int not null,
  foreign key(_id) references author(_id),
  foreign key(a_id) references article(a_id),
  primary key(_id, a_id)
) engine = innodb;

drop table if exists volume_article;
/*create table if not exists volume_article (
  _id int not null,
  volnum smallint not null,
  a_id int not null,
  pg_nos varchar(30),
  foreign key(_id, volnum) references volume(_id, volnum),
  foreign key(a_id) references article(a_id),
  primary key(_id, volnum)
) engine = innodb;
*/

-- changed to reflect volume key
-- pg_nos is a key because of at least one article that appears in two different sets of pages "parallel ll parsing"
-- the other option would be to merge the pages programatically
create table if not exists volume_article (
  _id int not null,
  volnum varchar(16) not null,
  vol_year smallint,
  a_id int not null,
  pg_nos varchar(30),
  foreign key(_id, volnum, vol_year) references volume(_id, volnum, vol_year),
  foreign key(a_id) references article(a_id),
  primary key(_id, volnum, vol_year, a_id, pg_nos)
) engine = innodb;


