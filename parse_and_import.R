#!/usr/bin/env Rscript
args = commandArgs(trailingOnly=TRUE)

if (length(args)==0) {
  stop("At least one argument must be supplied: directory of json files to be loaded.", call.=FALSE)
} else if (length(args) == 1) {
  json_dir = paste0("./", args[1], "/")
}

# loads packages suppressing messages
suppressPackageStartupMessages( expr = {
  library(jsonlite)
  library(purrr)
  library(dplyr)
  library(mongolite)
})

print_error_files = function(error_files){
  cat("\n\n======================================\n")
  cat("ERROR files list: \n")
  walk(error_files, ~ cat(.x, "\n"))
  cat("\n======================================\n\n")
}

# use the parameter passed to get the list of files to process
## test
json_dir = "./json_files/"

json_files = list.files(path = json_dir, pattern = "*.json$")
error_files = c()


articles = list(filename = character(), json = list(authors = list(), 
                                                    title = character(), 
                                                    year = numeric(), 
                                                    volume = numeric(), 
                                                    pages = character()))
processed = character(length = length(json_files))

# approx 37s
#for(i in seq_along(json_files)) {
for(i in 1:2) {
  cat("json_files[i]", json_files[i], "\n")
    json = tryCatch(read_json(paste0(json_dir,json_files[i])),
                    error = function(e) {
                                cat("\t\tERROR parsing file: ", json_files[i], "\n")
                                error_files <<- rbind(error_files, json_files[i])
                                print(e)
                                cat("======================================\n")
                                },
                    warning = function(w){
                      cat("\t\tWARNING parsing file: ", json_files[i], "\n")
                      error_files = rbind(error_files, json_files[i])
                      print(w)
                      cat("--------------------------------------------------\n")
                    }
    )
    print("before if")
    if(!is.null(json)){
      print("inside if")
      if(class(json$author[[1]]) == "list") {
        print("if")
        authors = map(json$author, ~ .x$ftext)
      } else {
        print("else")
        print(json$author[[1]])
        authors = list(json$author$ftext)
      }
      print("fora do else")
      year = if_else(is.null(json$year$ftext[1]), NA_integer_, as.integer(json$year$ftext[1]))
      title = if_else(is.null(json$title$ftext[1]), NA_character_, json$title$ftext[1])
      volume = if_else(is.null((json$volume$ftext[1])), NA_integer_, as.integer(json$volume$ftext[1]))
      pages = if_else(is.null(json$pages$ftext[1]), NA_character_, json$pages$ftext[1])
      
      articles$filename[[i]] = json_files[i]
      print("aqui")
      articles$json[[i]]$authors[[i]] = authors
      print("aqui")
      articles$json[[i]]$year = year
      print("aqui")
      articles$json[[i]]$title = title
      print("aqui")
      articles$json[[i]]$volume = volume
      print("aqui")
      articles$json[[i]]$pages = pages
      print("aqui")
    }
    processed[i] = json_files[i]
    if(i %% 10000 == 0) cat(date(), " - processed: ", i, "\n", sep = "")
    #print(articles)
#    tib_json = as_tibble(articles[i]) %>% select(-filename)
    print(articles$json[[i]])
    print(class(articles$json[[i]]))
    print("here")
    # print(articles[[i]])
    # print(class(articles[[i]]))
    # print(str(tib_json))
    # print(tib_json)
    # jsonlite::serializeJSON(tib_json, pretty = T)
    #jsonlite::stream_out(tib_json, con = stdout(), verbose = T, pagesize = 100)
}

articles$json[[1]]
#print_error_files(error_files)

# transforms the list into a tibble with a nested list for authors
#df = as_tibble(articles)

# filter invalid jsons (the ones that have a different format and have any NA fields
#invalid = df %>% filter(is.na(title) | is.na(year) | is.na(volume) | is.na(pages))
#invalid = df[!complete.cases(df %>% select(-author)), ]

#valid_json = df %>% na.omit()
#invalid_json = df %>% anti_join(valid_json, by = c("filename" = "filename"))

# if(nrow(df) == (nrow(valid_json) + nrow(invalid_json))){
#     cat("Saving valid and invalid json datasets.\n")
#     saveRDS(df, file = "all_json_files.RDS", compress = T)
#     saveRDS(valid_json, file = "valid_json_files.RDS", compress = T)
#     saveRDS(invalid_json, file = "invalid_json_files.RDS", compress = T)
# }

# connects to mongo to articles with articles collection
#articles = mongo(collection = "articles", url = "mongodb://127.0.0.1:30017/articles")

# writes the valid_json tibble to valid_articles.json file so it can be imported to mongo
# is there another way?
# ~7s
#jsonlite::stream_out(valid_json, file("valid_articles.json"), verbose = T, pagesize = 20000)
#jsonlite::stream_out(valid_json, con = stdout(), verbose = T, pagesize = 10)

# drops articles collection
#articles$drop()
# ~ 4.2s
#system.time(articles$import(file("valid_articles.json")))
#articles$count()


