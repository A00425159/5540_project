-- drop temp tables and load indexes
drop index mag_name on magazine;
drop index auth_name_idx on author;
drop index article_title on article;
drop index mag_name_idx on magazine_staging;
drop index auth_name_stg_idx on author_staging;
drop index articles_staging_auth_name_idx on articles_staging;
drop index articles_staging_title on articles_staging;

